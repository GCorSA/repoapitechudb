package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

//    Para recuperar la lista directamente ordenada
//    public List<UserModel> findAll() {
//        return this.userRepository.findAll(Sort.by(Sort.Direction.ASC, "age"));
//    }

    public List<UserModel> getUsers(String orderBy) {
        System.out.println("getUsers");

        List<UserModel> result;

        if (orderBy != null) {
            System.out.println("Se ha pedido ordenación");

            result = this.userRepository.findAll(Sort.by("age"));

        } else {
            result = this.userRepository.findAll();
        }

        return result;
    }

    public UserModel add(UserModel user) {
        System.out.println("add");

        return this.userRepository.save(user);
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("findById");
        System.out.println("La id del usuario a buscar es :" + id);

        return this.userRepository.findById(id);
    }

    public UserModel update(UserModel user) {
        System.out.println("update");

        return this.userRepository.save(user);
    }

    public Boolean delete(String id) {
        System.out.println("delete");

        boolean result = false;

        if (this.userRepository.findById(id).isPresent() == true) {
            System.out.println("He encontrado el usuario, borrando");
            this.userRepository.deleteById(id);

            result = true;
        }

        return result;
    }
}
