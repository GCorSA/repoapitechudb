package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.services.ProductService;
import com.techu.apitechudb.services.PurchaseService;
import com.techu.apitechudb.services.PurchaseServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apitechu/v2")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;


    // CREAMOS UNA NUEVA COMPRA
    @PostMapping("/purchases")
    public ResponseEntity<PurchaseServiceResponse> addPurchase(@RequestBody PurchaseModel purchase) {
        System.out.println("addPurchase");
        System.out.println("la id de la compra a añadir " + purchase.getId());
        System.out.println("La id del cliente que quiere comprar " + purchase.getUserId());
        System.out.println("Relación de productos comprados " + purchase.getPurchaseItems());

        PurchaseServiceResponse result = this.purchaseService.addPurchase(purchase);

        return new ResponseEntity<>(
                result,
                result.getResponseHttpStatusCode()
        );
    }

    //LISTAMOS LAS COMPRAS EXISTENTES
    @GetMapping("/purchases")
        public ResponseEntity<List<PurchaseModel>> getPurchases() {
        System.out.println("getPurchases");

        return new ResponseEntity<>(this.purchaseService.getPurchases(), HttpStatus.OK);
    }
}
