package com.techu.apitechudb.controllers;

import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.techu.apitechudb.models.ProductModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
//@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE}) xa la clase
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    // a nivel de método
    //@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE})
    public ResponseEntity<List<ProductModel>> getProducts() {
        System.out.println("getProducts");

        //return this.productService.findAll();
        return new ResponseEntity<>(this.productService.findAll(), HttpStatus.OK);
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product) {
        System.out.println("addProducts");
        System.out.println("La id del producto que se va a crear es " + product.getId());
        System.out.println("La descripción del producto que se va a crear es " + product.getDesc());
        System.out.println("El precio del producto que se va a crear es " + product.getPrice());

        return new ResponseEntity<>(this.productService.add(product), HttpStatus.CREATED);
    }

    @GetMapping("/products/{id}")
    //public ResponseEntity<ProductModel> getProductById(@PathVariable String id) {   con esto no funciona
    //public ResponseEntity<?> getProductById(@PathVariable String id) {
    public ResponseEntity<Object> getProductById(@PathVariable String id) {

        System.out.println("getProductById");
        System.out.println("la id del producto a buscar es " + id);

        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND

        );
// Si no queremos usar ternarios podemos usar un if else normal:
//        if (result.isPresent() == true) {
//            return new ResponseEntity<>(result.get(), HttpStatus.OK)
//        } else {
//            return  new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
//        }

//        return new ResponseEntity<>(new ProductModel(), HttpStatus.OK);
    }
    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel productModel, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("La id del producto que se va a actualizar en parámetro es " + id);
        System.out.println("La id del producto que se va a actualizar en parámetro es " + productModel.getId());
        System.out.println("La descripción del producto a actualizar en parámetro es " + productModel.getDesc());
        System.out.println("El precio del producto a actualizar en parámetro es " + productModel.getPrice());

        Optional<ProductModel> productToUpdate = this.productService.findById(id);

        if (productToUpdate.isPresent()) {
            System.out.println("Producto para actualizar encontrado, acutalizando");
            this.productService.update(productModel);
        }

        return new ResponseEntity(productModel,
                productToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProdudt(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar es " + id);

        boolean deletedProduct = this.productService.delete(id);

        return new ResponseEntity<>(
                deletedProduct ? "Producto borrado" : "Producto no borrado",
                deletedProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}
