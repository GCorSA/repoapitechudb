package com.techu.apitechudb.services;

import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.repositories.ProductRepository;
import com.techu.apitechudb.repositories.PurchaseRepository;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.w3c.dom.html.HTMLQuoteElement;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ProductRepository productRepository;

    //ALTA DE COMPRA
    public PurchaseServiceResponse addPurchase(PurchaseModel purchase) {
        System.out.println("addPurchase");

        PurchaseServiceResponse result = new PurchaseServiceResponse();

        result.setPurchase(purchase);

        //el usuario que está comprando debe existir en la BBDD de Users
        if (this.userRepository.findById(purchase.getUserId()).isEmpty() == true) {
            System.out.println("Usuario no encontrado");

            result.setMsg("El usuario de la compra no se ha encontrado");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        //Comprobamos que no existe ninguna otra compra con ese id
        if (this.purchaseRepository.findById(purchase.getId()).isPresent() == true) {
            System.out.println("Ya hay una compra con esa id");

            result.setMsg("Ya hay una compra con ese id");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        //Calculamos el importe final de la compra en función del número de artículos y sus precios correspondientes
        float amoutn = 0;
        for(Map.Entry<String, Integer> purchaseItem : purchase.getPurchaseItems().entrySet()) {
            if (this.productRepository.findById(purchaseItem.getKey()).isEmpty()) {
                System.out.println("El product con la id " + purchaseItem.getKey() + " no se ha encontrado");

                result.setMsg("El product con la id " + purchaseItem.getKey() + " no se ha encontrado");
                result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

                return result;
            } else {
                System.out.println("Añadiendo valor de " + purchaseItem.getValue() + " unidades de producto " +
                        "al total");

                amoutn +=
                        (this.productRepository.findById(purchaseItem.getKey()).get().getPrice() * purchaseItem.getValue());
            }
        }
        purchase.setAmount(amoutn);

        //Si todas las validaciones anteriores son superadas, insertamos la compra en BBDD con el AMOUNT calculado
        this.purchaseRepository.save(purchase);
        result.setMsg("Compra añadida correctamente");
        result.setResponseHttpStatusCode(HttpStatus.CREATED);

        return result;
    }

    //LISTAMOS COMPRAS
    public List<PurchaseModel> getPurchases() {
        System.out.println("getPurchases");

        return this.purchaseRepository.findAll();
    }
}