package com.techu.apitechudb.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "users")
public class UserModel {

    @Id
    private String id;
    private String name;
    private int age;

    // constructor sin parámetros
    public UserModel() {
    }

    // constructor con parámetros
    public UserModel(String id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    // get y set de id
    public String getId() {

        return this.id;
    }

    public void setId(String id) {
        this.id = id;

    }


    // get y set de name
    public String getName() {
        return this.name;

    }

    public void setName(String name) {

        this.name = name;
    }


    // get y set de age
    public int getAge() {

        return this.age;
    }

    public void setPrice(int age) {

        this.age = age;
    }
}