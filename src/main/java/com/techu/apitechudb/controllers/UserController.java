package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    @Autowired
    UserService userService;


//    @GetMapping("/users")
//    public ResponseEntity<List<UserModel>> getUsers() {
//        System.out.println("getUsers");
//
//        return new ResponseEntity<>(this.userService.findAll(), HttpStatus.OK);
//    }

    //GET desde /users
    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(
            @RequestParam(name = "$orderBy", required = false) String orderBy
    ) {
        System.out.println("getUsers");
        System.out.println("El valor de $orderBy es " + orderBy);

        return new ResponseEntity<>(this.userService.getUsers(orderBy), HttpStatus.OK);
    }


    //POST desde /users
    @PostMapping("/users")
    public ResponseEntity<UserModel> addUsers(@RequestBody UserModel user) {
        System.out.println("addUser");
        System.out.println("La id del usuario que se va a crear es " + user.getId());
        System.out.println("El nombre del cliente que se va a crear es " + user.getName());
        System.out.println("La edad del cliente que se va a crear es " + user.getAge());

        return new ResponseEntity<>(this.userService.add(user), HttpStatus.CREATED);
    }

    //GET con id
    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id) {

        System.out.println("getUserById");
        System.out.println("La id del usuario a buscar es " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
            result.isPresent() ? result.get() : "Usuario no encontrado",
            result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND

        );

    }

    //PUT un id
    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id) {
        System.out.println("updateUser");
        System.out.println("La id del usuario que se va a actualizar es " + id);
        System.out.println("La id del usuario que se va a actualizar es por parámetro " + user.getId());
        System.out.println("El nombre del usuario que se va a por parámetro es " + user.getName());
        System.out.println("La edad del usuario que se va a actualizar por parámetro es " + user.getAge());

        Optional<UserModel> userToUpdate = this.userService.findById(id);

        if (userToUpdate.isPresent()) {
            System.out.println("Usuario para actualizar encontrado, acutalizando");
            this.userService.update(user);
        }

        return new ResponseEntity(user,
                userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    //DELETE un id
    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id) {
        System.out.println("deleteUser");
        System.out.println("La id del usuario a borrar es " + id);

        boolean deletedUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deletedUser ? "Usuario borrado" : "Usuario no borrado",
                deletedUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}
